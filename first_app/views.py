from django.shortcuts import render
from django.http import HttpResponse
from . import views
# Create your views here.
'''we've created a dictionary that i will pass as a key value thing here
key = insert me
value =hello i am ...
ab we need a page to have my_dict's value there'''

def index(request):
    my_dict = {'insert_me':"Hello I am from index.html! and created by mr.devashish ",
               'second_input':"hi iam coming from inside the folder now"}

    return render(request,'first_app/index.html',context=my_dict)


